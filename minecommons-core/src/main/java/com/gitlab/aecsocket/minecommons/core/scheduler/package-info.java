/**
 * Utilities for scheduling tasks which run later, either as a one-off task or repeating.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.core.scheduler;
