/**
 * Minecommons Paper module, with utilities for the Paper platform.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper;
