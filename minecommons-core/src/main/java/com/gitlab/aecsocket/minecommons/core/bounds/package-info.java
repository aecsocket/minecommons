/**
 * Utilities for bounding boxes and 3D volumes.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.core.bounds;
