/**
 * Utilities for scheduling tasks which run later, either as a one-off task or repeating - Paper
 * platform implementations.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper.scheduler;
