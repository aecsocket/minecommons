/**
 * Utilities for base plugin setup, such as commands and plugin integrations.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper.plugin;
