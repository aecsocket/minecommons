/**
 * Polar coordinate system vectors.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.core.vector.polar;
