/**
 * Utilities for managing {@link org.bukkit.persistence.PersistentDataContainer}s.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper.persistence;
