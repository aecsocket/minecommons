/**
 * Paper wrappers for biome classes, also providing biome registry manipulation.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper.biome;
