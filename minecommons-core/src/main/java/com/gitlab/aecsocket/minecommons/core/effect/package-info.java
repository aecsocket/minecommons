/**
 * Visible effects which can be sent to players.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.core.effect;
