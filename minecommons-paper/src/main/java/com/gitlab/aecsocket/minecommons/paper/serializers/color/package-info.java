/**
 * Color type serializers - Paper platform implementations.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper.serializers.color;
