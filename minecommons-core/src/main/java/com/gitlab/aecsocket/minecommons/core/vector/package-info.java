/**
 * Utilities for vectors, including vectors in the Cartesian and Polar coordinate
 * systems.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.core.vector;
