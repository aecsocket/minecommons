/**
 * ProtocolLib class type serializers.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper.serializers.protocol;
