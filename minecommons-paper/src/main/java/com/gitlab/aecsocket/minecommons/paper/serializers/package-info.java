/**
 * Serialization utilities, using Configurate - Paper platform implementations.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper.serializers;
