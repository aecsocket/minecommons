/**
 * Paper implementations of the corresponding core package classes.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper.effect;
