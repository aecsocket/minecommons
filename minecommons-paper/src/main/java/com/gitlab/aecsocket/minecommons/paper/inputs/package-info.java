/**
 * Utilities for intercepting player inputs through various methods.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper.inputs;
