/**
 * Utilities for event dispatching and listening.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.core.event;
