/**
 * Classes for localization and translation.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.core.i18n;
