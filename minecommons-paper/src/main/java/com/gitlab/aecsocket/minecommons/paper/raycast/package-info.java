/**
 * Utilities for raycasting and custom implementation of testing collisions between objects and bounds - Paper
 * platform implementations.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.paper.raycast;
