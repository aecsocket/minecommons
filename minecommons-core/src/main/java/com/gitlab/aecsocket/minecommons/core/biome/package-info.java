/**
 * Utilities and wrappers for biomes.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.core.biome;
