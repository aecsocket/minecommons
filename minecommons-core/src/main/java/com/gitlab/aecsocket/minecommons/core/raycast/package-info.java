/**
 * Utilities for raycasting and custom implementation of testing collisions between objects and bounds.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.minecommons.core.raycast;
